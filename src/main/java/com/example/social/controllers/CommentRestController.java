package com.example.social.controllers;

import com.example.social.models.dtos.CommentDTO;
import com.example.social.models.dtos.CreateCommentDTO;
import com.example.social.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/comments")
public class CommentRestController {
    private CommentService commentService;

    @Autowired
    public CommentRestController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/new")
    public void comments(HttpServletRequest httpServletRequest, @RequestParam int postId, @RequestParam String text) {
        commentService.create(httpServletRequest, postId, text);
    }

    @PutMapping("/delete")
    public void comments(HttpServletRequest httpServletRequest, @RequestParam int postId, @RequestParam int commentId) {
        commentService.delete(httpServletRequest, postId, commentId);
    }

    @GetMapping
    public ResponseEntity<List<CommentDTO>> comments(@RequestParam int postId) {
        List<CommentDTO> commentDTOS = commentService.get(postId);
        return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
    }
}
