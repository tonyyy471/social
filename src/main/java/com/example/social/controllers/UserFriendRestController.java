package com.example.social.controllers;

import com.example.social.models.dtos.UserFriendDTO;
import com.example.social.models.dtos.UserSmallDTO;
import com.example.social.services.UserFriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/userFriends")
public class UserFriendRestController {
    private UserFriendService userFriendService;

    @Autowired
    public UserFriendRestController(UserFriendService userFriendService) {
        this.userFriendService = userFriendService;
    }

    @GetMapping
    public ResponseEntity<List<UserSmallDTO>> userFriends(String username) {
        List<UserSmallDTO> userSmallDTOS = userFriendService.userFriends(username);
        return new ResponseEntity<>(userSmallDTOS, HttpStatus.OK);
    }

    @PutMapping("/delete")
    public void userFriends(HttpServletRequest httpServletRequest, @RequestParam String username) {
        userFriendService.delete(httpServletRequest, username);
    }
}
