package com.example.social.controllers;

import com.example.social.models.dtos.UserSmallDTO;
import com.example.social.services.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/likes")
public class LikeRestController {
    private LikeService likeService;

    @Autowired
    public LikeRestController(LikeService likeService) {
        this.likeService = likeService;
    }

    @GetMapping("/new")
    public void likes(HttpServletRequest httpServletRequest, @RequestParam int postId) {
        likeService.createOrDelete(httpServletRequest, postId);
    }

    @GetMapping
    public ResponseEntity<List<UserSmallDTO>> likes(@RequestParam Integer postId) {
        List<UserSmallDTO> likes = likeService.get(postId);
        return new ResponseEntity<>(likes, HttpStatus.OK);
    }
}
