package com.example.social.controllers;

import com.example.social.models.dtos.*;
import com.example.social.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/users")
public class UserRestController {
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public void users(@RequestBody CreateUserDTO createUserDTO) {
        userService.register(createUserDTO);
    }

    @GetMapping("/edit")
    public void users(HttpServletRequest httpServletRequest, @RequestParam String password, String fullName, String bio) {
        userService.edit(httpServletRequest, password, fullName, bio);
    }

    @PutMapping("/uploadAvatar")
    public void users(HttpServletRequest httpServletRequest, @RequestParam(name = "file", required = false) MultipartFile file) {
        userService.uploadAvatar(httpServletRequest, file);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JwtTokenDTO> users(@RequestBody LoginDTO loginDTO) {
        JwtTokenDTO jwtTokenDTO = userService.login(loginDTO);
        return new ResponseEntity<>(jwtTokenDTO, HttpStatus.OK);
    }

    @GetMapping("/single")
    public ResponseEntity<UserBigDTO> users(HttpServletRequest httpServletRequest, @RequestParam String username) {
        UserBigDTO userBigDTO = userService.get(httpServletRequest, username);
        return new ResponseEntity<>(userBigDTO, HttpStatus.OK);
    }
}
