package com.example.social.controllers;

import com.example.social.models.dtos.BioDTO;
import com.example.social.models.dtos.PostDTO;
import com.example.social.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/posts")
public class PostRestController {
    private PostService postService;

    //Learn mockito and create tests!!!
    @Autowired
    public PostRestController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/new")
    public ResponseEntity<Integer> posts(HttpServletRequest httpServletRequest, @RequestParam(name = "file", required = false) MultipartFile file) {
        int postId = postService.create(httpServletRequest, file);
        return new ResponseEntity<>(postId, HttpStatus.OK);
    }

    @PutMapping("/bio")
    public void posts(@RequestBody BioDTO bioDTO) {
        postService.addBio(bioDTO);
    }

    @PutMapping("/delete")
    public void posts(HttpServletRequest httpServletRequest, @RequestParam int id) {
        postService.delete(httpServletRequest, id);
    }

    @GetMapping("/single")
    public ResponseEntity<List<PostDTO>> posts(String username) {
        List<PostDTO> postDTOs = postService.get(username);
        return new ResponseEntity<>(postDTOs, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<PostDTO>> posts(HttpServletRequest httpServletRequest) {
        List<PostDTO> postDTOs = postService.get(httpServletRequest);
        return new ResponseEntity<>(postDTOs, HttpStatus.OK);
    }
}
