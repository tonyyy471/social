package com.example.social.controllers;

import com.example.social.models.dtos.FriendRequestDTO;
import com.example.social.services.FriendRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/friendRequests")
public class FriendRequestController {
    private FriendRequestService friendRequestService;

    @Autowired
    public FriendRequestController(FriendRequestService friendRequestService) {
        this.friendRequestService = friendRequestService;
    }

    @GetMapping
    public ResponseEntity<List<FriendRequestDTO>> friendRequests(String username) {
        List<FriendRequestDTO> friendRequests = friendRequestService.get(username);
        return new ResponseEntity<>(friendRequests, HttpStatus.OK);
    }

    @GetMapping("/requestFriendship")
    public void friendRequests(HttpServletRequest httpServletRequest, @RequestParam String username) {
        friendRequestService.create(httpServletRequest, username);
    }

    @GetMapping("/manage")
    public void friendRequests(HttpServletRequest httpServletRequest, @RequestParam String username, @RequestParam boolean approve) {
        friendRequestService.manage(httpServletRequest, username, approve);
    }
}
