package com.example.social.repositories;

import com.example.social.models.FileId;

public interface FileRepository {
    FileId get();

    void update(FileId fileId);

    void save(FileId fileId);
}
