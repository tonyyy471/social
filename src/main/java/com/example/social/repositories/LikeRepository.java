package com.example.social.repositories;

import com.example.social.models.Like;
import com.example.social.models.PostLike;

import java.util.List;

public interface LikeRepository {
    List<PostLike> get(int postId);

    void save(Like like, PostLike postLike);

    void delete(PostLike postLike, Like like);
}
