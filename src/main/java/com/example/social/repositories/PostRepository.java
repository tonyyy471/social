package com.example.social.repositories;

import com.example.social.models.Post;
import com.example.social.models.User;
import com.example.social.models.UserPost;

import java.util.List;

public interface PostRepository {
    int save(Post post);

    Post get(int postId);

    void update(Post post);

    void save(UserPost userPost);

    UserPost get(User user, Post post);

    void delete(UserPost userPost, Post post);

    UserPost getUserPost(int postId);

    List<UserPost> get(User user);
}
