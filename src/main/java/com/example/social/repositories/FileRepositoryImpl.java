package com.example.social.repositories;

import com.example.social.models.FileId;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FileRepositoryImpl implements FileRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public FileRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public FileId get() {
        FileId fileId;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<FileId> query = session.createQuery("from FileId", FileId.class);
            fileId = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return fileId;
    }

    @Override
    public void update(FileId fileId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(fileId);
            session.getTransaction().commit();
        }
    }

    @Override
    public void save(FileId fileId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(fileId);
            session.getTransaction().commit();
        }
    }
}
