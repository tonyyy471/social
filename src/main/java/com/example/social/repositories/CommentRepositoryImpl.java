package com.example.social.repositories;

import com.example.social.models.Comment;
import com.example.social.models.PostComment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Comment comment, PostComment postComment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(comment);
            session.save(postComment);
            session.getTransaction().commit();
        }
    }

    @Override
    public PostComment get(int postId, int commentId) {
        PostComment postComment;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<PostComment> query = session
                    .createQuery("from PostComment where post.id = :postId and comment.id = :commentId", PostComment.class);
            query.setParameter("postId", postId);
            query.setParameter("commentId", commentId);
            postComment = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return postComment;
    }

    @Override
    public void delete(PostComment postComment, Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(postComment);
            session.delete(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<PostComment> get(int postId) {
        List<PostComment> postComments;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<PostComment> query = session
                    .createQuery("from PostComment where post.id = :postId", PostComment.class);
            query.setParameter("postId", postId);
            postComments = query.list();
            session.getTransaction().commit();
        }
        return postComments;
    }
}
