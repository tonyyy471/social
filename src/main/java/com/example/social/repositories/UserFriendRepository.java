package com.example.social.repositories;

import com.example.social.models.User;
import com.example.social.models.UserFriend;

import java.util.List;

public interface UserFriendRepository {
    UserFriend get(User requestReceiver, User requestSender);

    void save(UserFriend userFriend);

    List<UserFriend> get(User user);

    void delete(UserFriend userFriend);
}
