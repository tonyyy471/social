package com.example.social.repositories;

import com.example.social.models.Post;
import com.example.social.models.User;
import com.example.social.models.UserPost;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PostRepositoryImpl implements PostRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int save(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(post);
            session.getTransaction().commit();
        }
        return post.getId();
    }

    @Override
    public Post get(int postId) {
        Post post;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Post> query = session.createQuery("from Post where id = :postId", Post.class);
            query.setParameter("postId", postId);
            post = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return post;
    }

    @Override
    public void update(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void save(UserPost userPost) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userPost);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserPost get(User user, Post post) {
        UserPost userPost;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<UserPost> query = session
                    .createQuery("from UserPost where user.id = :userId and post.id = :postId", UserPost.class);
            query.setParameter("userId", user.getId());
            query.setParameter("postId", post.getId());
            userPost = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return userPost;
    }

    @Override
    public void delete(UserPost userPost, Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userPost);
            session.delete(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserPost getUserPost(int postId) {
        UserPost userPost;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<UserPost> query = session
                    .createQuery("from UserPost where post.id = :postId", UserPost.class);
            query.setParameter("postId", postId);
            userPost = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return userPost;
    }

    @Override
    public List<UserPost> get(User user) {
        List<UserPost> userPosts;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<UserPost> query = session.createQuery("from UserPost where user.id = :userId", UserPost.class);
            query.setParameter("userId", user.getId());
            userPosts = query.list();
            session.getTransaction().commit();
        }
        return userPosts;
    }
}
