package com.example.social.repositories;

import com.example.social.models.Comment;
import com.example.social.models.PostComment;

import java.util.List;

public interface CommentRepository {
    void save(Comment comment, PostComment postComment);

    PostComment get(int postId, int commentId);

    void delete(PostComment postComment, Comment comment);

    List<PostComment> get(int postId);
}
