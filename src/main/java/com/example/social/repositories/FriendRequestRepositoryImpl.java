package com.example.social.repositories;

import com.example.social.models.FriendRequest;
import com.example.social.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FriendRequestRepositoryImpl implements FriendRequestRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public FriendRequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public FriendRequest get(User requestReceiver, User requestSender) {
        FriendRequest friendRequest;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<FriendRequest> query = session
                    .createQuery("from FriendRequest where requestReceiver.id = :requestReceiverId " +
                            "and requestSender.id = :requestSenderId", FriendRequest.class);
            query.setParameter("requestReceiverId", requestReceiver.getId());
            query.setParameter("requestSenderId", requestSender.getId());
            friendRequest = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return friendRequest;
    }

    @Override
    public void save(FriendRequest newFriendRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(newFriendRequest);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<FriendRequest> get(User user) {
        List<FriendRequest> friendRequests;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<FriendRequest> query = session
                    .createQuery("from FriendRequest where requestReceiver.id = :requestReceiverId", FriendRequest.class);
            query.setParameter("requestReceiverId", user.getId());
            friendRequests = query.list();
        }
        return friendRequests;
    }

    @Override
    public void delete(FriendRequest friendRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(friendRequest);
            session.getTransaction().commit();
        }
    }
}
