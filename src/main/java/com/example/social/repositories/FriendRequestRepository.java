package com.example.social.repositories;

import com.example.social.models.FriendRequest;
import com.example.social.models.User;

import java.util.List;

public interface FriendRequestRepository {
    FriendRequest get(User requestReceiver, User requestSender);

    void save(FriendRequest newFriendRequest);

    List<FriendRequest> get(User user);

    void delete(FriendRequest friendRequest);
}
