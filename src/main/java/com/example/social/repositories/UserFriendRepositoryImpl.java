package com.example.social.repositories;

import com.example.social.models.FriendRequest;
import com.example.social.models.User;
import com.example.social.models.UserFriend;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserFriendRepositoryImpl implements UserFriendRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserFriendRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserFriend get(User user, User friend) {
        UserFriend userFriend;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<UserFriend> query = session
                    .createQuery("from UserFriend where user.id = :userId " +
                            "and friend.id = :friendId", UserFriend.class);
            query.setParameter("userId", user.getId());
            query.setParameter("friendId", friend.getId());
            userFriend = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return userFriend;
    }

    @Override
    public void save(UserFriend userFriend) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userFriend);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<UserFriend> get(User user) {
        List<UserFriend> userFriends;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<UserFriend> query = session
                    .createQuery("from UserFriend where user.id = :userId ", UserFriend.class);
            query.setParameter("userId", user.getId());
            userFriends = query.list();
            session.getTransaction().commit();
        }
        return userFriends;
    }

    @Override
    public void delete(UserFriend userFriend) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userFriend);
            session.getTransaction().commit();
        }
    }
}
