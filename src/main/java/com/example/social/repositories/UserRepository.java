package com.example.social.repositories;

import com.example.social.models.User;

public interface UserRepository {
    User get(String username);

    void save(User user);

    void update(User user);
}
