package com.example.social.repositories;

import com.example.social.models.Like;
import com.example.social.models.PostComment;
import com.example.social.models.PostLike;
import com.example.social.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LikeRepositoryImpl implements LikeRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public LikeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PostLike> get(int postId) {
        List<PostLike> postLikes;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<PostLike> query = session
                    .createQuery("from PostLike where post.id = :postId", PostLike.class);
            query.setParameter("postId", postId);
            postLikes = query.list();
            session.getTransaction().commit();
        }
        return postLikes;
    }

    @Override
    public void save(Like like, PostLike postLike) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(like);
            session.save(postLike);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(PostLike postLike, Like like) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(postLike);
            session.delete(like);
            session.getTransaction().commit();
        }
    }
}
