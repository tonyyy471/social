package com.example.social.services;

import com.example.social.models.FriendRequest;
import com.example.social.models.dtos.FriendRequestDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FriendRequestService {
    void create(HttpServletRequest httpServletRequest, String username);

    List<FriendRequestDTO> get(String username);

    void manage(HttpServletRequest httpServletRequest, String username, boolean approve);
}
