package com.example.social.services;

import com.example.social.models.FriendRequest;
import com.example.social.models.ModelsMapper;
import com.example.social.models.User;
import com.example.social.models.UserFriend;
import com.example.social.models.dtos.FriendRequestDTO;
import com.example.social.repositories.FriendRequestRepository;
import com.example.social.repositories.UserFriendRepository;
import com.example.social.repositories.UserRepository;
import com.example.social.security.AuthorizedUser;
import com.example.social.services.validators.FriendRequestValidator;
import com.example.social.services.validators.UserFriendValidator;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class FriendRequestServiceImpl implements FriendRequestService {
    private UserRepository userRepository;
    private FriendRequestRepository friendRequestRepository;
    private UserFriendRepository userFriendRepository;
    private FileService fileService;

    @Autowired
    public FriendRequestServiceImpl(UserRepository userRepository, FileService fileService,
                                    FriendRequestRepository friendRequestRepository, UserFriendRepository userFriendRepository) {
        this.userRepository = userRepository;
        this.fileService = fileService;
        this.friendRequestRepository = friendRequestRepository;
        this.userFriendRepository = userFriendRepository;
    }

    @Override
    public void create(HttpServletRequest httpServletRequest, String username) {
        User requestSender = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(requestSender);

        User requestReceiver = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(requestReceiver);

        FriendRequestValidator.getInstance().requestSenderMustNotBeRequestReceiver(requestSender, requestReceiver);

        FriendRequest friendRequest = friendRequestRepository
                .get(requestReceiver, requestSender);
        FriendRequestValidator.getInstance().friendRequestMustBeNull(friendRequest);

        UserFriend userFriend = userFriendRepository
                .get(requestReceiver, requestSender);
        UserFriendValidator.getInstance().userFriendMustBeNull(userFriend);

        FriendRequest newFriendRequest = ModelsMapper.getInstance().getFriendRequest(requestReceiver, requestSender);

        friendRequestRepository.save(newFriendRequest);
    }

    @Override
    public List<FriendRequestDTO> get(String username) {
        User user = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(user);

        return ModelsMapper.getInstance().friendRequestsToFriendRequestDTOs(friendRequestRepository.get(user), fileService);
    }

    @Override
    public void manage(HttpServletRequest httpServletRequest, String username, boolean approve) {
        User requestReceiver = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(requestReceiver);

        User requestSender = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(requestSender);

        FriendRequest friendRequest = friendRequestRepository
                .get(requestReceiver, requestSender);

        FriendRequestValidator.getInstance().friendRequestMustNotBeNull(friendRequest);

        if (approve) {
            if (userFriendRepository.get(requestReceiver, requestSender) == null &&
                    userFriendRepository.get(requestSender, requestReceiver) == null) {
                userFriendRepository.save(new UserFriend(requestReceiver, requestSender));
                userFriendRepository.save(new UserFriend(requestSender, requestReceiver));
            }
        }

        friendRequestRepository.delete(friendRequest);
    }
}
