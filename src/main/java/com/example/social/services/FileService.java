package com.example.social.services;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    String store(MultipartFile file, boolean fileIsAvatar);

    byte[] get(String image);
}
