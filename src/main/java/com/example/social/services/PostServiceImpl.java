package com.example.social.services;

import com.example.social.models.*;
import com.example.social.models.dtos.BioDTO;
import com.example.social.models.dtos.CommentDTO;
import com.example.social.models.dtos.PostDTO;
import com.example.social.models.dtos.UserSmallDTO;
import com.example.social.repositories.*;
import com.example.social.security.AuthorizedUser;
import com.example.social.services.validators.PostValidator;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;
    private FileService fileService;
    private UserRepository userRepository;
    private LikeService likeService;
    private CommentService commentService;
    private UserFriendRepository userFriendRepository;
    private CommentRepository commentRepository;
    private LikeRepository likeRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, FileService fileService, UserRepository userRepository,
                           LikeService likeService, CommentService commentService,
                           UserFriendRepository userFriendRepository, CommentRepository commentRepository,
                           LikeRepository likeRepository) {
        this.postRepository = postRepository;
        this.fileService = fileService;
        this.userRepository = userRepository;
        this.likeService = likeService;
        this.commentService = commentService;
        this.userFriendRepository = userFriendRepository;
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
    }

    @Override
    public int create(HttpServletRequest httpServletRequest, MultipartFile file) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        String image = fileService.store(file, false);
        Post post = new Post(image);
        int postId = postRepository.save(post);
        postRepository.save(new UserPost(user, post));

        return postId;
    }

    @Override
    public void addBio(BioDTO bioDTO) {
        Post post = postRepository.get(bioDTO.getPostId());
        PostValidator.getInstance().postMustNotBeNull(post);

        post.setBio(bioDTO.getText());

        postRepository.update(post);
    }

    @Override
    public void delete(HttpServletRequest httpServletRequest, int id) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        Post post = postRepository.get(id);
        PostValidator.getInstance().postMustNotBeNull(post);

        UserPost userPost = postRepository.get(user, post);
        PostValidator.getInstance().userPostMustNotBeNull(userPost);

        List<PostComment> postComments = commentRepository.get(id);
        for (PostComment postComment : postComments) {
            commentRepository.delete(postComment, postComment.getComment());
        }

        List<PostLike> postLikes = likeRepository.get(id);
        for (PostLike postLike : postLikes) {
            likeRepository.delete(postLike, postLike.getLike());
        }

        postRepository.delete(userPost, post);
    }

    @Override
    public List<PostDTO> get(String username) {
        User user = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(user);

        List<UserPost> userPosts = postRepository.get(user);
        List<PostDTO> posts = new ArrayList<>();
        for (UserPost userPost : userPosts) {
            PostDTO postDTO = ModelsMapper.getInstance().getPostDTO(userPost, fileService, likeService, commentService);
            posts.add(postDTO);
        }

        posts.sort((o1, o2) -> commentService.sort(o1.getDate(), o2.getDate()));

        return posts;
    }

    @Override
    public List<PostDTO> get(HttpServletRequest httpServletRequest) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        List<UserFriend> userFriends = userFriendRepository.get(user);

        List<PostDTO> posts = new ArrayList<>();

        for (UserFriend userFriend : userFriends) {
            List<PostDTO> friendPosts = get(userFriend.getFriend().getUsername());
            posts.addAll(friendPosts);
        }
        posts.addAll(get(user.getUsername()));

        posts.sort((o1, o2) -> commentService.sort(o1.getDate(), o2.getDate()));

        return posts;
    }
}
