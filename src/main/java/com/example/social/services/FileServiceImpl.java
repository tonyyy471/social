package com.example.social.services;

import com.example.social.constants.Constants;
import com.example.social.models.FileId;
import com.example.social.models.User;
import com.example.social.repositories.FileRepository;
import com.example.social.repositories.UserRepository;
import com.example.social.services.exceptions.FileException;
import com.example.social.services.exceptions.NotFoundException;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileServiceImpl implements FileService {
    private Path fileLocation;
    private FileRepository fileRepository;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository) {
        this.fileLocation = Paths.get(Constants.PATH)
                .toAbsolutePath().normalize();
        this.fileRepository = fileRepository;

        try {
            Files.createDirectories(this.fileLocation);
        } catch (Exception ex) {
            throw new FileException(Constants.DIRECTORY_NOT_CREATED, ex);
        }
    }

    @Override
    public String store(MultipartFile file, boolean fileIsAvatar) {
        boolean updateFileId = false;

        FileId fileId = fileRepository.get();
        if (fileId == null) {
            fileId = new FileId(0);
            fileRepository.save(fileId);
        } else {
            updateFileId = true;
            fileId.setValue(fileId.getValue() + 1);
        }

        String fileName = fileId.getValue() + " " + StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new FileException(Constants.INVALID_FILE_NAME + fileName);
            }

            Path targetLocation = this.fileLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileException(Constants.UPLOAD_FAILED, ex);
        }

        if (updateFileId) {
            fileRepository.update(fileId);
        }

        return fileName;
    }

    @Override
    public byte[] get(String fileName) {
        if (fileName != null) {
            try {
                File image = loadFileAsResource(fileName);
                InputStream inputStream = new FileInputStream(image);
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                int data = inputStream.read();

                while (data != -1) {
                    buffer.write(data);
                    data = inputStream.read();
                }

                inputStream.close();

                return buffer.toByteArray();
            } catch (IOException e) {
                throw new NotFoundException(Constants.FILE_NOT_FOUND);
            }
        }
        return null;
    }

    private File loadFileAsResource(String fileName) throws IOException {
        try {
            Path location = Paths.get(Constants.PATH).toAbsolutePath().normalize();
            Path filePath = location.resolve(fileName).normalize();

            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource.getFile();
            } else {
                throw new NotFoundException(Constants.FILE_NOT_FOUND);
            }
        } catch (MalformedURLException ex) {
            throw new NotFoundException(Constants.FILE_NOT_FOUND);
        }
    }
}
