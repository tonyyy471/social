package com.example.social.services;

import com.example.social.models.ModelsMapper;
import com.example.social.models.User;
import com.example.social.models.UserFriend;
import com.example.social.models.dtos.UserFriendDTO;
import com.example.social.models.dtos.UserSmallDTO;
import com.example.social.repositories.UserFriendRepository;
import com.example.social.repositories.UserRepository;
import com.example.social.security.AuthorizedUser;
import com.example.social.services.validators.UserFriendValidator;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserFriendServiceImpl implements UserFriendService {
    private UserFriendRepository userFriendRepository;
    private UserRepository userRepository;
    private FileService fileService;

    @Autowired
    public UserFriendServiceImpl(UserFriendRepository userFriendRepository, UserRepository userRepository,
                                 FileService fileService) {
        this.userFriendRepository = userFriendRepository;
        this.userRepository = userRepository;
        this.fileService = fileService;
    }

    @Override
    public List<UserSmallDTO> userFriends(String username) {
        User user = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(user);

        List<UserFriend> userFriends = userFriendRepository.get(user);
        List<UserSmallDTO> userSmallDTOS = new ArrayList<>();

        for (UserFriend userFriend : userFriends) {
            userSmallDTOS.add(new UserSmallDTO(userFriend.getFriend().getUsername(),
                    fileService.get(userFriend.getFriend().getAvatar())));
        }

        return userSmallDTOS;
    }

    @Override
    public void delete(HttpServletRequest httpServletRequest, String username) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        User friend = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(friend);

        UserFriend userFriend = userFriendRepository
                .get(user, friend);
        UserFriendValidator.getInstance().userFriendMustNotBeNull(userFriend);

        UserFriend friendUser = userFriendRepository
                .get(friend, user);
        UserFriendValidator.getInstance().userFriendMustNotBeNull(friendUser);

        userFriendRepository.delete(userFriend);
        userFriendRepository.delete(friendUser);
    }
}
