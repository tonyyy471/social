package com.example.social.services;


import com.example.social.models.dtos.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    void register(CreateUserDTO createUserDTO);

    void edit(HttpServletRequest httpServletRequest, String password, String fullName, String bio);

    void uploadAvatar(HttpServletRequest httpServletRequest, MultipartFile file);

    JwtTokenDTO login(LoginDTO loginDTO);

    UserBigDTO get(HttpServletRequest httpServletRequest, String username);
}
