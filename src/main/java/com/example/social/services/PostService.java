package com.example.social.services;

import com.example.social.models.dtos.BioDTO;
import com.example.social.models.dtos.PostDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PostService {
    int create(HttpServletRequest httpServletRequest, MultipartFile file);

    void addBio(BioDTO bioDTO);

    void delete(HttpServletRequest httpServletRequest, int id);

    List<PostDTO> get(String username);

    List<PostDTO> get(HttpServletRequest httpServletRequest);
}
