package com.example.social.services;

import com.example.social.models.*;
import com.example.social.models.dtos.UserSmallDTO;
import com.example.social.repositories.*;
import com.example.social.security.AuthorizedUser;
import com.example.social.services.validators.LikeValidator;
import com.example.social.services.validators.PostValidator;
import com.example.social.services.validators.UserFriendValidator;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class LikeServiceImpl implements LikeService {
    private LikeRepository likeRepository;
    private PostRepository postRepository;
    private UserFriendRepository userFriendRepository;
    private FileService fileService;

    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository, PostRepository postRepository,
                           UserFriendRepository userFriendRepository, FileService fileService) {
        this.likeRepository = likeRepository;
        this.postRepository = postRepository;
        this.userFriendRepository = userFriendRepository;
        this.fileService = fileService;
    }

    @Override
    public void createOrDelete(HttpServletRequest httpServletRequest, int postId) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        List<PostLike> postLikes = likeRepository.get(postId);
        PostLike postLike = LikeValidator.getInstance().postIsLiked(postLikes, user);

        if (postLike == null) {
            Like like = new Like(user);
            likeRepository.save(like, new PostLike(postRepository.getUserPost(postId).getPost(), like));
        } else {
            likeRepository.delete(postLike, postLike.getLike());
        }
    }

    @Override
    public List<UserSmallDTO> get(Integer postId) {
        List<PostLike> postLikes = likeRepository.get(postId);
        List<UserSmallDTO> likes = new ArrayList<>();

        for (PostLike postLike : postLikes) {
            likes.add(new UserSmallDTO(postLike.getLike().getLikeGiver().getUsername(),
                    fileService.get(postLike.getLike().getLikeGiver().getAvatar())));
        }
        return likes;
    }
}
