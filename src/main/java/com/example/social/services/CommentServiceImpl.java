package com.example.social.services;

import com.example.social.models.*;
import com.example.social.models.dtos.CommentDTO;
import com.example.social.models.dtos.CreateCommentDTO;
import com.example.social.repositories.CommentRepository;
import com.example.social.repositories.PostRepository;
import com.example.social.repositories.UserFriendRepository;
import com.example.social.repositories.UserRepository;
import com.example.social.security.AuthorizedUser;
import com.example.social.services.validators.CommentValidator;
import com.example.social.services.validators.PostValidator;
import com.example.social.services.validators.UserFriendValidator;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private CommentRepository commentRepository;
    private PostRepository postRepository;
    private UserFriendRepository userFriendRepository;
    private FileService fileService;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, PostRepository postRepository,
                              UserFriendRepository userFriendRepository, FileService fileService) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.userFriendRepository = userFriendRepository;
        this.fileService = fileService;
    }

    @Override
    public void create(HttpServletRequest httpServletRequest, int postId, String text) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        UserPost userPost = postRepository.getUserPost(postId);
        PostValidator.getInstance().userPostMustNotBeNull(userPost);

        CommentValidator.getInstance().commentTextShouldNotBeNullOrEmpty(text);

        if (user.getId() != userPost.getUser().getId()) {
            UserFriend userFriend = userFriendRepository.get(user, userPost.getUser());
            UserFriendValidator.getInstance().userFriendMustNotBeNull(userFriend);
        }

        Comment comment = new Comment(user, text);
        PostComment postComment = new PostComment(userPost.getPost(), comment);

        commentRepository.save(comment, postComment);
    }

    @Override
    public void delete(HttpServletRequest httpServletRequest, int postId, int commentId) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(user);

        UserPost userPost = postRepository.getUserPost(postId);
        PostValidator.getInstance().userPostMustNotBeNull(userPost);

        PostComment postComment = commentRepository.get(postId, commentId);
        CommentValidator.getInstance().postCommentMustNotBeNull(postComment);

        if (userPost.getUser().getId() == user.getId()) {
            commentRepository.delete(postComment, postComment.getComment());
            return;
        }

        UserFriend userFriend = userFriendRepository.get(user, userPost.getUser());
        UserFriendValidator.getInstance().userFriendMustNotBeNull(userFriend);

        CommentValidator.getInstance().userMustBeCommentAuthor(user, postComment.getComment().getCommentGiver());

        commentRepository.delete(postComment, postComment.getComment());
    }

    @Override
    public List<CommentDTO> get(int postId) {
        List<PostComment> postComments = commentRepository.get(postId);
        List<CommentDTO> commentDTOS = new ArrayList<>();

        for (PostComment postComment : postComments) {
            commentDTOS.add(ModelsMapper.getInstance().postCommentToCommentDTO(postComment, fileService));
        }

        commentDTOS.sort((o1, o2) -> sort(o2.getDate(), o1.getDate()));

        return commentDTOS;
    }

    @Override
    public int sort(Date date, Date date2) {
        if (date.before(date2)) {
            return 1;
        }
        return -1;
    }
}
