package com.example.social.services;

import com.example.social.models.dtos.UserFriendDTO;
import com.example.social.models.dtos.UserSmallDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserFriendService {

    List<UserSmallDTO> userFriends(String username);

    void delete(HttpServletRequest httpServletRequest, String username);
}
