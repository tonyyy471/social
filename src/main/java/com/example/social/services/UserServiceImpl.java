package com.example.social.services;

import com.example.social.constants.Constants;
import com.example.social.models.ModelsMapper;
import com.example.social.models.User;
import com.example.social.models.dtos.*;
import com.example.social.models.enums.Role;
import com.example.social.repositories.UserRepository;
import com.example.social.security.AuthorizedUser;
import com.example.social.security.JwtTokenProvider;
import com.example.social.services.exceptions.UnauthorizedException;
import com.example.social.services.validators.FriendRequestValidator;
import com.example.social.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private FileService fileService;
    private AuthenticationManager authenticationManager;
    private JwtTokenProvider jwtTokenProvider;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, FileService fileService, AuthenticationManager authenticationManager,
                           JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.fileService = fileService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void register(CreateUserDTO createUserDTO) {
        UserValidator.getInstance().userMustBeNull(userRepository.get(createUserDTO.getUsername()));
        UserValidator.getInstance().validateFields(createUserDTO.getUsername(), createUserDTO.getPassword());

        User user = ModelsMapper.getInstance().createUserDtoToUser(createUserDTO, passwordEncoder);
        userRepository.save(user);
    }

    @Override
    public void edit(HttpServletRequest httpServletRequest, String password, String fullName, String bio) {
        User authenticatedUser = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(authenticatedUser);

        ModelsMapper.getInstance().editUser(password, fullName, bio, authenticatedUser, passwordEncoder);
        userRepository.update(authenticatedUser);
    }

    @Override
    public void uploadAvatar(HttpServletRequest httpServletRequest, MultipartFile file) {
        User authenticatedUser = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(authenticatedUser);
        String avatar = fileService.store(file, true);
        authenticatedUser.setAvatar(avatar);
        userRepository.update(authenticatedUser);
    }

    @Override
    public JwtTokenDTO login(LoginDTO loginDTO) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
            return jwtTokenProvider.createToken(loginDTO.getUsername(), Role.provideRoles(loginDTO.getUsername()));
        } catch (AuthenticationException e) {
            throw new UnauthorizedException(Constants.INVALID_USERNAME_OR_PASSWORD);
        }
    }

    @Override
    public UserBigDTO get(HttpServletRequest httpServletRequest, String username) {
        User authenticatedUser = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UserValidator.getInstance().userMustNotBeNull(authenticatedUser);

        User user = userRepository.get(username);
        UserValidator.getInstance().userMustNotBeNull(user);

        return new UserBigDTO(user.getUsername(), fileService.get(user.getAvatar()), user.getName(), user.getBio());
    }
}
