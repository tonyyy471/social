package com.example.social.services.validators;

import com.example.social.constants.Constants;
import com.example.social.models.User;
import com.example.social.services.exceptions.IllegalActionException;
import com.example.social.services.exceptions.NotFoundException;

public class UserValidator {
    private static UserValidator ourInstance;

    public static UserValidator getInstance() {
        if (ourInstance == null) {
            ourInstance = new UserValidator();
        }
        return ourInstance;
    }

    public void userMustBeNull(User user) {
        if (user != null) {
            throw new IllegalActionException(Constants.USERNAME_EXISTS);
        }
    }

    public void userMustNotBeNull(User user) {
        if (user == null) {
            throw new NotFoundException(Constants.USER_DOES_NOT_EXIST);
        }
    }

    public void validateFields(String username, String password) {
        if (username == null || username.equals("") || password == null || password.equals("")) {
            throw new IllegalActionException(Constants.INVALID_USERNAME_OR_PASSWORD);
        }
    }
}
