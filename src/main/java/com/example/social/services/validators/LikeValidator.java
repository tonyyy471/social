package com.example.social.services.validators;

import com.example.social.models.PostLike;
import com.example.social.models.User;

import java.util.List;

public class LikeValidator {
    private static LikeValidator ourInstance;

    public static LikeValidator getInstance() {
        if (ourInstance == null) {
            ourInstance = new LikeValidator();
        }
        return ourInstance;
    }

    public PostLike postIsLiked(List<PostLike> postLikes, User user) {
        for (PostLike postLike : postLikes) {
            if (postLike.getLike().getLikeGiver().getId() == user.getId()) {
                return postLike;
            }
        }
        return null;
    }
}
