package com.example.social.services.validators;

import com.example.social.constants.Constants;
import com.example.social.models.Post;
import com.example.social.models.User;
import com.example.social.models.UserPost;
import com.example.social.services.exceptions.NotFoundException;

public class PostValidator {
    private static PostValidator ourInstance;

    public static PostValidator getInstance() {
        if (ourInstance == null){
            ourInstance = new PostValidator();
        }
        return ourInstance;
    }

    public void postMustNotBeNull(Post post) {
        if (post == null) {
            throw new NotFoundException(Constants.POST_NOT_FOUND);
        }
    }

    public void userPostMustNotBeNull(UserPost userPost) {
        if (userPost == null) {
            throw new NotFoundException(Constants.POST_NOT_FOUND);
        }
    }
}
