package com.example.social.services.validators;

import com.example.social.constants.Constants;
import com.example.social.models.UserFriend;
import com.example.social.services.exceptions.IllegalActionException;
import com.example.social.services.exceptions.NotFoundException;

public class UserFriendValidator {
    private static UserFriendValidator ourInstance;

    public static UserFriendValidator getInstance() {
        if (ourInstance == null) {
            ourInstance = new UserFriendValidator();
        }
        return ourInstance;
    }


    public void userFriendMustBeNull(UserFriend userFriend) {
        if (userFriend != null) {
            throw new IllegalActionException(Constants.USER_ALREADY_FRIEND);
        }
    }

    public void userFriendMustNotBeNull(UserFriend userFriend) {
        if (userFriend == null) {
            throw new NotFoundException(Constants.USER_NOT_FOUND);
        }
    }
}
