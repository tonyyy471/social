package com.example.social.services.validators;

import com.example.social.constants.Constants;
import com.example.social.models.PostComment;
import com.example.social.models.User;
import com.example.social.models.dtos.CreateCommentDTO;
import com.example.social.services.exceptions.IllegalActionException;
import com.example.social.services.exceptions.NotFoundException;

public class CommentValidator {
    private static CommentValidator ourInstance;

    public static CommentValidator getInstance() {
        if (ourInstance == null) {
            ourInstance = new CommentValidator();
        }
        return ourInstance;
    }


    public void postCommentMustNotBeNull(PostComment postComment) {
        if (postComment == null) {
            throw new NotFoundException(Constants.COMMENT_NOT_FOUND);
        }
    }

    public void userMustBeCommentAuthor(User commentPoster, User commentGiver) {
        if (commentPoster.getId() != commentGiver.getId()) {
            throw new IllegalActionException(Constants.YOU_ARE_NOT_COMMENT_AUTHOR);
        }
    }

    public void commentTextShouldNotBeNullOrEmpty(String text) {
        if (text == null || text.equals("")){
            throw new IllegalActionException(Constants.COMMENT_HAS_NO_TEXT);
        }
    }
}
