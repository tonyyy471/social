package com.example.social.services.validators;

import com.example.social.constants.Constants;
import com.example.social.models.FriendRequest;
import com.example.social.models.User;
import com.example.social.services.exceptions.IllegalActionException;
import com.example.social.services.exceptions.NotFoundException;

public class FriendRequestValidator {
    private static FriendRequestValidator ourInstance;

    public static FriendRequestValidator getInstance() {
        if (ourInstance == null){
            ourInstance = new FriendRequestValidator();
        }
        return ourInstance;
    }


    public void friendRequestMustBeNull(FriendRequest friendRequest) {
        if (friendRequest != null){
            throw new IllegalActionException(Constants.REQUEST_SENT);
        }
    }

    public void friendRequestMustNotBeNull(FriendRequest friendRequest) {
        if (friendRequest == null){
            throw new NotFoundException(Constants.REQUEST_NOT_FOUND);
        }
    }

    public void requestSenderMustNotBeRequestReceiver(User requestSender, User requestReceiver) {
        if (requestSender.getId() == requestReceiver.getId()){
            throw new IllegalActionException(Constants.YOU_CANNOT_SEND_REQUEST_TO_YOURSELF);
        }
    }
}
