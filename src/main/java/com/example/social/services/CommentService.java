package com.example.social.services;

import com.example.social.models.dtos.CommentDTO;
import com.example.social.models.dtos.CreateCommentDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

public interface CommentService {
    void create(HttpServletRequest httpServletRequest, int postId, String text);

    void delete(HttpServletRequest httpServletRequest, int postId, int commentId);

    List<CommentDTO> get(int postId);

    int sort(Date date, Date date2);
}
