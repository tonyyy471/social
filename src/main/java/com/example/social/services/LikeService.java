package com.example.social.services;

import com.example.social.models.dtos.UserSmallDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LikeService {
    void createOrDelete(HttpServletRequest httpServletRequest, int postId);

    List<UserSmallDTO> get(Integer postId);
}
