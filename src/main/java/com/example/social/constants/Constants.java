package com.example.social.constants;

import java.net.URI;

public class Constants {
    public static final String USERNAME_EXISTS = "Username already exists!";
    public static final String USER_DOES_NOT_EXIST = "User does not exist!";
    public static final String REQUEST_SENT = "Request already sent!";
    public static final String USER_ALREADY_FRIEND = "User is already in friend list!";
    public static final String REQUEST_NOT_FOUND = "Request not found!";
    public static final String USER_NOT_FOUND = "User not found!";
    public static final String DIRECTORY_NOT_CREATED = "Could not createOrDelete the directory!";
    public static final String INVALID_FILE_NAME = "Invalid file name";
    public static final String UPLOAD_FAILED = "Upload failed!";
    public static final String POST_NOT_FOUND = "Post not found!";
    public static final String COMMENT_NOT_FOUND = "Comment not found!";
    public static final String YOU_ARE_NOT_COMMENT_AUTHOR = "You are not comment author!";
    public static final String FILE_NOT_FOUND = "File not found!";
    public static final String INVALID_USERNAME_OR_PASSWORD = "Invalid username or password!";
    public static final String YOU_CANNOT_SEND_REQUEST_TO_YOURSELF = "You cannot send request to yourself!";
    public static final String PATH = "images";
    public static final String COMMENT_HAS_NO_TEXT = "Comment has no text!";
}
