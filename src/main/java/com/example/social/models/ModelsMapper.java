package com.example.social.models;

import com.example.social.models.dtos.*;
import com.example.social.services.CommentService;
import com.example.social.services.FileService;
import com.example.social.services.LikeService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelsMapper {
    private static ModelsMapper ourInstance;

    public static ModelsMapper getInstance() {
        if (ourInstance == null) {
            ourInstance = new ModelsMapper();
        }
        return ourInstance;
    }

    public List<UserFriendDTO> userFriendsToUserFriendsDTO(List<UserFriend> userFriends) {
        List<UserFriendDTO> userFriendDTOS = new ArrayList<>();
        for (UserFriend userFriend : userFriends) {
            userFriendDTOS.add(new UserFriendDTO(userFriend.getFriend().getUsername()));
        }
        return userFriendDTOS;
    }

    public PostDTO getPostDTO(UserPost userPost, FileService fileService, LikeService likeService,
                              CommentService commentService) {
        UserSmallDTO author = new UserSmallDTO(userPost.getUser().getUsername(),
                fileService.get(userPost.getUser().getAvatar()));
        byte[] image = fileService.get(userPost.getPost().getImage());
        Date date = userPost.getPost().getDate();
        String bio = userPost.getPost().getBio();
        List<UserSmallDTO> likes = likeService.get(userPost.getPost().getId());
        List<CommentDTO> comments = commentService.get(userPost.getPost().getId());
        return new PostDTO(userPost.getPost().getId(), author, image, date, bio, likes, comments);
    }

    public User createUserDtoToUser(CreateUserDTO createUserDTO, PasswordEncoder passwordEncoder) {
        User user = new User(createUserDTO.getUsername(), passwordEncoder.encode(createUserDTO.getPassword()));
        user.setAvatar("user.png");
        return user;
    }

    public void editUser(String password, String fullName, String bio, User user, PasswordEncoder passwordEncoder) {
        if (password != null && !password.equals("")) {
            user.setPassword(passwordEncoder.encode(password));
        }
        if (fullName != null) {
            user.setName(fullName);
        }
        if (bio != null) {
            user.setBio(bio);
        }
    }

    public FriendRequest getFriendRequest(User requestReceiver, User requestSender) {
        return new FriendRequest(requestReceiver, requestSender);
    }

    public List<FriendRequestDTO> friendRequestsToFriendRequestDTOs(List<FriendRequest> friendRequests, FileService fileService) {
        List<FriendRequestDTO> friendRequestDTOS = new ArrayList<>();

        for (FriendRequest friendRequest : friendRequests) {
            User requestSender = friendRequest.getRequestSender();
            friendRequestDTOS.add(new FriendRequestDTO(friendRequest.getId(), new UserSmallDTO(requestSender.getUsername(),
                    fileService.get(requestSender.getAvatar()))));
        }
        return friendRequestDTOS;
    }

    public CommentDTO postCommentToCommentDTO(PostComment postComment, FileService fileService) {
        Comment comment = postComment.getComment();
        User user = comment.getCommentGiver();
        UserSmallDTO userSmallDTO = new UserSmallDTO(user.getUsername(), fileService.get(user.getAvatar()));
        return new CommentDTO(userSmallDTO, comment.getText(), comment.getDate());
    }
}
