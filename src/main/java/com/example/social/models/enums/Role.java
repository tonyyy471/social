package com.example.social.models.enums;

import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public enum Role implements GrantedAuthority {
    ROLE_ADMIN, ROLE_USER;

    @Override
    public String getAuthority() {
        return name();
    }

    public static List<Role> provideRoles(String email) {
        List<Role> roles = new ArrayList<>();
        roles.add(ROLE_USER);
        return roles;
    }
}
