package com.example.social.models;

import javax.persistence.*;

@Entity
@Table(name = "fileid")
public class FileId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private long value;

    public FileId() {
    }

    public FileId(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
