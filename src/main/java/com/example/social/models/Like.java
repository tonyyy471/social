package com.example.social.models;

import javax.persistence.*;

@Entity
@Table(name = "likes")
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private User likeGiver;

    public Like() {
    }

    public Like(User likeGiver) {
        this.likeGiver = likeGiver;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getLikeGiver() {
        return likeGiver;
    }

    public void setLikeGiver(User likeGiver) {
        this.likeGiver = likeGiver;
    }
}
