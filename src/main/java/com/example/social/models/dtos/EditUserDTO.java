package com.example.social.models.dtos;

public class EditUserDTO {
    private String password;
    private String name;
    private String bio;

    public EditUserDTO() {
    }

    public EditUserDTO(String password, String name, String bio) {
        this.password = password;
        this.name = name;
        this.bio = bio;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
