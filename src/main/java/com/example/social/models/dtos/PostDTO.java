package com.example.social.models.dtos;

import java.util.Date;
import java.util.List;

public class PostDTO {
    private int id;
    private UserSmallDTO author;
    private byte[] image;
    private Date date;
    private String bio;
    private List<UserSmallDTO> likes;
    private List<CommentDTO> comments;

    public PostDTO() {
    }

    public PostDTO(int id, UserSmallDTO author, byte[] image, Date date, String bio, List<UserSmallDTO> likes, List<CommentDTO> comments) {
        this.id = id;
        this.author = author;
        this.image = image;
        this.date = date;
        this.bio = bio;
        this.likes = likes;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserSmallDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserSmallDTO author) {
        this.author = author;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public List<UserSmallDTO> getLikes() {
        return likes;
    }

    public void setLikes(List<UserSmallDTO> likes) {
        this.likes = likes;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }
}
