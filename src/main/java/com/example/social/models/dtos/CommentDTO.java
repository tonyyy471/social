package com.example.social.models.dtos;

import java.util.Date;

public class CommentDTO {
    private UserSmallDTO author;
    private String text;
    private Date date;

    public CommentDTO() {
    }

    public CommentDTO(UserSmallDTO author, String text, Date date) {
        this.author = author;
        this.text = text;
        this.date = date;
    }

    public UserSmallDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserSmallDTO author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

