package com.example.social.models.dtos;

public class FriendRequestDTO {
    private int id;
    private UserSmallDTO requestSender;

    public FriendRequestDTO() {
    }

    public FriendRequestDTO(int id, UserSmallDTO requestSender) {
        this.id = id;
        this.requestSender = requestSender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserSmallDTO getRequestSender() {
        return requestSender;
    }

    public void setRequestSender(UserSmallDTO requestSender) {
        this.requestSender = requestSender;
    }
}
