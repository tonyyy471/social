package com.example.social.models.dtos;

public class CreateCommentDTO {
    private int postId;
    private String text;

    public CreateCommentDTO() {
    }

    public CreateCommentDTO(int postId, String text) {
        this.postId = postId;
        this.text = text;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
