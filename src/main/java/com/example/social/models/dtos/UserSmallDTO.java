package com.example.social.models.dtos;

public class UserSmallDTO {
    private String username;
    private byte[] avatar;

    public UserSmallDTO() {
    }

    public UserSmallDTO(String username, byte[] avatar) {
        this.username = username;
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }
}
