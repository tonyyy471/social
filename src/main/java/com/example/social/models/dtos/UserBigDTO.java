package com.example.social.models.dtos;

public class UserBigDTO {
    private String username;
    private byte[] avatar;
    private String name;
    private String bio;

    public UserBigDTO() {
    }

    public UserBigDTO(String username, byte[] avatar, String name, String bio) {
        this.username = username;
        this.avatar = avatar;
        this.name = name;
        this.bio = bio;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
