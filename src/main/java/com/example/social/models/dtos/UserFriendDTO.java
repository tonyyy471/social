package com.example.social.models.dtos;

public class UserFriendDTO {
    private String username;

    //private byte[] image...

    public UserFriendDTO() {
    }

    public UserFriendDTO(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
