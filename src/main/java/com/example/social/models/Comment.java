package com.example.social.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private User commentGiver;
    @NotNull
    private String text;
    @NotNull
    private Date date;

    public Comment() {
    }

    public Comment(User commentGiver, @NotNull String text) {
        this.commentGiver = commentGiver;
        this.text = text;
        this.date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCommentGiver() {
        return commentGiver;
    }

    public void setCommentGiver(User commentGiver) {
        this.commentGiver = commentGiver;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
