var app = angular.module("loginApp", []);

app.controller("loginController", function ($scope, $http) {
    $scope.loginErrorMessage = false;

    $scope.authenticate = function () {
        var username = $scope.loginUsername;
        var password = $scope.loginPassword;

        $http.post("http://localhost:8080/api/users/authenticate", {
            username: username,
            password: password
        }).then(function (response) {
            localStorage.setItem("jwtToken", response.data.token);
            localStorage.setItem("loggedUserUsername", username);
            window.location.href = '/main.html';
        }, function () {
            $scope.loginErrorMessage = true;
        })
    };

    $scope.hideLoginErrorMessage = function () {
        $scope.loginErrorMessage = false;
    }
});