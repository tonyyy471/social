var app = angular.module("singleUserApp", []);

app.controller("singleUserController", function ($scope, $http) {
    $scope.logOut = function () {
        localStorage.clear();
        window.location = "index.html";
    };

    $scope.loadResources = function () {
        if (localStorage.getItem("jwtToken") === null) {
            window.location = "/index.html"
        }
        $scope.getUser(localStorage.getItem("singleUserUsername"));
        $scope.getUserFriends(localStorage.getItem("singleUserUsername"));
    };

    $scope.userIsSelfOrFriend = function (friends) {
        if (localStorage.getItem("loggedUserUsername") === localStorage.getItem("singleUserUsername")) {
            return true;
        }
        for (var i = 0; i < friends.length; i++) {
            if (localStorage.getItem("loggedUserUsername") === friends[i].username) {
                return true;
            }
        }
        return false;
    };

    $scope.showImage = function (id) {
        var element = document.getElementById("img" + id);
        element.style.display = "block";
    };

    $scope.getUser = function (username) {
        $http.get("http://localhost:8080/api/users/single", {
            params: {
                username: username
            },
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            }
        }).then(function (response) {
            $scope.singleUser = response.data;
            checkBioAndName($scope.singleUser.bio, $scope.singleUser.name);

            if (localStorage.getItem("loggedUserUsername") === username) {
                $scope.loggedUser = response.data;
            }
        }, function (response) {
        });
    };

    $scope.getSingleUserPosts = function () {
        $http.get("http://localhost:8080/api/posts/single", {
            params: {
                username: localStorage.getItem("singleUserUsername")
            }
        }).then(function (response) {
            $scope.singleUserPosts = response.data;
        }, function () {
        });
    };

    $scope.showUserPostsCount = true;

    $scope.showAddFriendButton = false;

    $scope.showMessageWhenAddingAsFriendIsUnavailableMessage = false;

    $scope.getUserFriends = function (username) {
        $http.get("http://localhost:8080/api/userFriends", {
            params: {
                username: username
            }
        }).then(function (response) {
            $scope.friends = response.data;

            if ($scope.userIsSelfOrFriend(response.data)) {
                $scope.getSingleUserPosts();
                if ($scope.userIsSelf() === true) {
                    $scope.getUserFriendRequests(localStorage.getItem("singleUserUsername"));
                }
            } else {
                $scope.showUserPostsCount = false;
                $scope.showAddFriendButton = true;
                $scope.userIsNotSelfNorFriend = true;
                $scope.getUserFriendRequests(localStorage.getItem("singleUserUsername"));
            }
        }, function () {
        });
    };

    $scope.userFriendRequests = [];

    $scope.userIsNotSelfNorFriend = null;

    $scope.showUserFriendRequests = function () {
        return $scope.userIsSelf() && $scope.userFriendRequests.length > 0;
    };

    $scope.getUserFriendRequests = function (username) {
        $http.get("http://localhost:8080/api/friendRequests", {
            params: {
                username: username
            }
        }).then(function (response) {
            if ($scope.userIsNotSelfNorFriend === true) {
                $scope.checkIfUserHaveSentYouARequest(JSON.parse(localStorage.getItem("loggedUserFriendRequests")));
                $scope.checkIfYouHaveSentARequest(response.data);
            }
            $scope.userFriendRequests = response.data;
        }, function (response) {
        });
    };

    $scope.checkIfYouHaveSentARequest = function (friendRequests) {
        for (var i = 0; i < friendRequests.length; i++) {
            if (friendRequests[i].requestSender.username === localStorage.getItem("loggedUserUsername")) {
                $scope.messageWhenAddingAsFriendIsUnavailable =
                    "You have sent " + localStorage.getItem("singleUserUsername") + " a friend request";
                $scope.hideAddFriendButtonAndShowFriendshipSentMessage();
                break;
            }
        }
    };

    $scope.checkIfUserHaveSentYouARequest = function (loggedUserFriendRequests) {
        for (var i = 0; i < loggedUserFriendRequests.length; i++) {
            if (loggedUserFriendRequests[i].requestSender.username === localStorage.getItem("singleUserUsername")) {
                $scope.messageWhenAddingAsFriendIsUnavailable =
                    localStorage.getItem("singleUserUsername") + " have sent you a friend request";
                $scope.hideAddFriendButtonAndShowFriendshipSentMessage();
                break;
            }
        }
    };

    $scope.hideAddFriendButtonAndShowFriendshipSentMessage = function () {
        $scope.showMessageWhenAddingAsFriendIsUnavailableMessage = true;
        $scope.showAddFriendButton = false;
    };

    $scope.messageWhenAddingAsFriendIsUnavailable = "";

    $scope.userIsSelf = function () {
        return localStorage.getItem("singleUserUsername") === localStorage.getItem("loggedUserUsername");
    };

    $scope.postUploadForm = false;

    $scope.showPostUploadForm = function () {
        $scope.postUploadForm = true;
    };

    $scope.goToUploadForm = function () {
        window.location = "upload-post.html"
    };

    var checkBioAndName = function (bio, name) {
        if (bio !== null) {
            $scope.showBio = true;
        }
        if (name !== null) {
            $scope.showName = true;
        }
    };

    $scope.sendFriendRequest = function (username) {
        $http.get("http://localhost:8080/api/friendRequests/requestFriendship", {
            params: {
                username: username
            },
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            }
        }).then(function () {
            location.reload();
        }, function () {
        });
    };

    $scope.goToSingleUser = function (username) {
        localStorage.setItem("singleUserUsername", username);
        window.location = "single-user.html";
    };

    $scope.friendRequests = false;

    $scope.showUserFriends = false;

    $scope.showRequests = function () {
        $scope.friendRequests = true;
    };

    $scope.showFriends = function () {
        $scope.showUserFriends = true;
    };

    $scope.manageFriendRequest = function (username, approve) {
        $http.get("http://localhost:8080/api/friendRequests/manage", {
            params: {
                username: username,
                approve: approve
            },
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            }
        }).then(function () {
            location.reload();
        }, function () {
        });
    };

    $scope.getColor = function (likes) {
        for (var i = 0; i < likes.length; i++) {
            if (likes[i].username === localStorage.getItem("loggedUserUsername")) {
                return 'red';
            }
        }
        return 'white';
    };

    $scope.likeUnlikePost = function (post) {
        $http.get("http://localhost:8080/api/likes/new", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            },
            params: {
                postId: post.id
            }
        }).then(function () {
            window.location.reload();
        }, function () {
        });
    };

    $scope.showOrHideComments = function (id) {
        var element = document.getElementById(id);

        if (element.style.display === "none") {
            element.style.display = "block";
        } else {
            element.style.display = "none";
        }
    };

    $scope.addNewComment = function (postId, commentsLenght) {
        var text = document.getElementById("newComment" + postId + commentsLenght).value;

        $http.get("http://localhost:8080/api/comments/new", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken"),
                "Content-Type": "application/json"
            },
            params: {
                postId: postId,
                text: text
            }
        }).then(function () {
            location.reload();
        }, function () {
            $scope.emptyCommentErrorMessage = true;
        });
    };

    $scope.emptyCommentErrorMessage = false;

    $scope.hideEmptyCommentErrorMessage = function () {
        $scope.emptyCommentErrorMessage = false;
    };

    $scope.loggedUser = JSON.parse(localStorage.getItem("loggedUser"));

    $scope.showMyProfile = false;

    $scope.showProfile = function () {
        $scope.showMyProfile = true;
    };

    $scope.submitUser = function () {
        var password = $scope.newPassword;
        var fullName = $scope.newFullName;
        var bio = $scope.newBio;
        var avatar = $("#avatar")[0].files[0];

        $scope.updateUser(password, fullName, bio);

        if (avatar !== undefined) {
            uploadFile(avatar);
        }

        location.reload();
    };

    $scope.updateUser = function (password, fullName, bio) {
        if (password === undefined) {
            password = "";
        }
        if (fullName === undefined) {
            fullName = $scope.loggedUser.name;
        }
        if (bio === null) {
            bio = loggedUser.bio;
        }

        $http.get("http://localhost:8080/api/users/edit", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            },
            params: {
                password: password,
                fullName: fullName,
                bio: bio
            }
        }).then(function () {
        }, function () {
            $("#showError").css("display", "block");
        });
    };

    var uploadFile = function (image) {
        var data = new FormData();
        data.append("file", image);
        $.ajax({
            type: "PUT",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/api/users/uploadAvatar",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("jwtToken")
            },
            success: function () {
            },
            error: function () {
                $("#showError").css("display", "block");
            }
        });
    };

    $scope.hideRegisterErrorMessage = function () {
        $("#showError").css("display", "none");
    };
});
