var app = angular.module("registerApp", []);

app.controller("registerController", function ($scope, $http) {
    $scope.hideRegisterErrorMessage = function () {
        $("#showError").css("display", "none");
    };

    $scope.submitUser = function () {
        var username = $scope.newUsername;
        var password = $scope.newPassword;
        var fullName = $scope.newFullName;
        var bio = $scope.newBio;
        var avatar = $("#avatar")[0].files[0];

        $scope.registerUser(username, password, fullName, bio, avatar);
    };

    $scope.registerUser = function (username, password, fullName, bio, avatar) {
        $http.post("http://localhost:8080/api/users/register", {
            username: username,
            password: password
        }).then(function () {
            $scope.authenticate(username, password, fullName, bio, avatar);
        }, function () {
            $("#showError").css("display", "block");
        })
    };

    $scope.addFullNameAndBio = function (fullName, bio) {
        $http.get("http://localhost:8080/api/users/edit", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            },
            params: {
                password: "",
                fullName: fullName,
                bio: bio
            }
        }).then(function () {
        }, function () {
            $("#showError").css("display", "block");
        });
    };

    var uploadFile = function (image) {
        var data = new FormData();
        data.append("file", image);
        $.ajax({
            type: "PUT",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/api/users/uploadAvatar",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("jwtToken")
            },
            success: function () {
            },
            error: function () {
                $("#showError").css("display", "block");
            }
        });
    };

    $scope.authenticate = function (username, password, fullName, bio, avatar) {
        $http.post("http://localhost:8080/api/users/authenticate", {
            username: username,
            password: password
        }).then(function (response) {
            localStorage.setItem("jwtToken", response.data.token);
            localStorage.setItem("loggedUserUsername", username);
            if (fullName !== undefined || bio !== undefined) {
                $scope.addFullNameAndBio(fullName, bio);
            }
            if (avatar !== undefined) {
                uploadFile(avatar);
            }
            window.location.href = '/main.html';
        }, function () {
            $("#showError").css("display", "block");
        })
    };
});