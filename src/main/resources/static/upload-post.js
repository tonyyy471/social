var app = angular.module("uploadPostApp", []);

app.controller("uploadPostController", function ($scope, $http) {

    $scope.hideUploadPostErrorMessage = function () {
        $("#showError").css("display", "none");
    };

    $scope.uploadPost = function () {
        var image = $("#file")[0].files[0];
        uploadFile(image);
    };

    var uploadFile = function (image) {
        var data = new FormData();
        data.append("file", image);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/api/posts/new",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("jwtToken")
            },
            success: function (response) {
                if ($scope.postBio !== undefined) {
                    addPostBio(response, $scope.postBio);
                } else {
                    window.location.href = "single-user.html";
                }
            },
            error: function () {
                $("#showError").css("display", "block");
            }
        });
    };

    var addPostBio = function (postId, text) {
        $http.put("http://localhost:8080/api/posts/bio", {
            postId: postId,
            text: text
        }).then(function () {
            window.location.href = "single-user.html";
        }, function () {
        })
    };

    $scope.logOut = function () {
        localStorage.clear();
        window.location = "index.html";
    };

    $scope.goToSingleUser = function (username) {
        localStorage.setItem("singleUserUsername", username);
        window.location = "single-user.html";
    };

    $scope.loggedUser = JSON.parse(localStorage.getItem("loggedUser"));
});