var app = angular.module("mainApp", []);

app.controller("mainController", function ($scope, $http) {

    $scope.showImage = function (id) {
        var element = document.getElementById("img" + id);
        element.style.display = "block";
    };

    $scope.showOrHideComments = function (id) {
        var element = document.getElementById(id);

        if (element.style.display === "none") {
            element.style.display = "block";
        } else {
            element.style.display = "none";
        }
    };

    $scope.emptyCommentErrorMessage = false;

    $scope.hideEmptyCommentErrorMessage = function () {
        $scope.emptyCommentErrorMessage = false;
    };

    $scope.hideNoSuchUserErrorMessage = function () {
        $("#noSuchUserErrorMessage").css("display", "none");
    };

    $scope.addNewComment = function (postId, commentsLenght) {
        var text = document.getElementById("newComment" + postId + commentsLenght).value;

        $http.get("http://localhost:8080/api/comments/new", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken"),
                "Content-Type": "application/json"
            },
            params: {
                postId: postId,
                text: text
            }
        }).then(function () {
            location.reload();
        }, function () {
            $scope.emptyCommentErrorMessage = true;
        });
    };

    $scope.loadResources = function () {
        if (localStorage.getItem("jwtToken") === null) {
            window.location = "/index.html"
        }
        $scope.getUser(localStorage.getItem("loggedUserUsername"), false);
        $scope.getUserFriends(localStorage.getItem("loggedUserUsername"));
        $scope.getPosts();
        $scope.getUserFriendRequests(localStorage.getItem("loggedUserUsername"));
    };

    $scope.getUserFriendRequests = function (username) {
        $http.get("http://localhost:8080/api/friendRequests", {
            params: {
                username: username
            }
        }).then(function (response) {
            localStorage.setItem("loggedUserFriendRequests", JSON.stringify(response.data));
        }, function () {
        });
    };

    $scope.goToSingleUser = function (username) {
        localStorage.setItem("singleUserUsername", username);
        window.location = "single-user.html";
    };

    $scope.getUser = function (username, searchNewFriend) {
        $http.get("http://localhost:8080/api/users/single", {
            params: {
                username: username
            },
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            }
        }).then(function (response) {
            if (searchNewFriend) {
                $scope.goToSingleUser(username);
            } else {
                localStorage.setItem("loggedUser", JSON.stringify(response.data));
                $scope.loggedUser = response.data;
            }
        }, function (response) {
            if (searchNewFriend) {
                $("#noSuchUserErrorMessage").css("display", "block");
            }
        });
    };

    $scope.getUserFriends = function (username) {
        $http.get("http://localhost:8080/api/userFriends", {
            params: {
                username: username
            }
        }).then(function (response) {
            $scope.friends = response.data;
        }, function () {
        });
    };

    $scope.getPosts = function () {
        $http.get("http://localhost:8080/api/posts/all", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            }
        }).then(function (response) {
            $scope.allPosts = response.data;
        }, function () {
        });
    };

    $scope.getColor = function (likes) {
        for (var i = 0; i < likes.length; i++) {
            if (likes[i].username === localStorage.getItem("loggedUserUsername")) {
                return 'red';
            }
        }
        return 'white';
    };

    $scope.likeUnlikePost = function (post) {
        $http.get("http://localhost:8080/api/likes/new", {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwtToken")
            },
            params: {
                postId: post.id
            }
        }).then(function () {
            window.location.reload();
        }, function () {
        });
    };

    $scope.logOut = function () {
        localStorage.clear();
        window.location = "index.html";
    };

    $scope.showPostLikes = false;

    $scope.getLikes = function (likes) {
        $scope.postLikes = likes;
        $scope.showPostLikes = true;
    }
});